# Useful Resources & Links

- Official Flutter Docs: [https://flutter.io/docs/](https://flutter.io/docs/)
- macOS Setup Guide: [https://flutter.io/setup-macos](https://flutter.io/setup-macos)
- Windows Setup Guide: [https://flutter.io/setup-windows](https://flutter.io/setup-windows)
- Linux Setup Guide: [https://flutter.io/setup-linux](https://flutter.io/setup-linux)
- Visual Studio Code: [https://code.visualstudio.com/](https://code.visualstudio.com/)
- Visual Studio Code Flutter Extension: [https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter](https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter)
- Android Studio: [https://developer.android.com/studio/](https://developer.android.com/studio/)
