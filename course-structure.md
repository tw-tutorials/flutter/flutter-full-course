# Course Structure

1. Getting started
2. Basics
3. Debugging
4. Mode Widgets, Styling, App Logic
5. Responsive & Adaptive UIs
6. Widgets & Flutter Internals
7. Navigation & Multiple Screens
8. State Management
9. User Input & Forms
10. Sending HTTP Requests
11. User Authentication
12. Animations
13. User Device Features (Camera, Maps, ...)
14. Running Native Device Code
15. Publish an App
16. Roundup & "How to Develop Great Apps"
