class Page {
  final String title;
  final Object widget;

  const Page({this.widget, this.title});
}
