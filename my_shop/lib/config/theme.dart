import 'package:flutter/material.dart';
import 'package:my_shop/helpers/custom_route.dart';

ThemeData theme = ThemeData(
  primarySwatch: Colors.purple,
  accentColor: Colors.deepOrange,
  fontFamily: 'Lato',
  pageTransitionsTheme: PageTransitionsTheme(builders: {
    TargetPlatform.android: CustomPageTransitonBuilder(),
    TargetPlatform.iOS: CustomPageTransitonBuilder(),
    TargetPlatform.fuchsia: CustomPageTransitonBuilder(),
  }),
);
