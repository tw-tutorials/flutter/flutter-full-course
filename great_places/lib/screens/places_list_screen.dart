import 'package:flutter/material.dart';
import 'package:great_places/providers/great_places.dart';
import 'package:great_places/screens/add_place_screen.dart';
import 'package:great_places/screens/place_detail_screen.dart';
import 'package:provider/provider.dart';

class PlacesListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Places'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(AddPlaceScreen.routeName);
            },
          )
        ],
      ),
      body: FutureBuilder(
        future: Provider.of<GreatPlaces>(context, listen: false)
            .fetchAndSetPlaces(),
        builder: (ctx, snapshot) => snapshot.connectionState ==
                ConnectionState.waiting
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Consumer<GreatPlaces>(
                child: Center(
                  child: const Text('Got no places yet.\nStart adding some!'),
                ),
                builder: (ctx, data, child) => data.items.length <= 0
                    ? child
                    : ListView.builder(
                        itemCount: data.items.length,
                        itemBuilder: (ctx, i) => ListTile(
                          leading: Hero(
                            tag: '${data.items[i].id}-image',
                            child: CircleAvatar(
                              backgroundImage: FileImage(data.items[i].image),
                            ),
                          ),
                          title: Text(data.items[i].title),
                          subtitle: Text(data.items[i].location.address),
                          onTap: () {
                            //TODO go to details page
                            Navigator.of(context).pushNamed(
                                PlaceDetailScreen.routeName,
                                arguments: data.items[i].id);
                          },
                        ),
                      ),
              ),
      ),
    );
  }
}
